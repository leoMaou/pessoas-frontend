# pessoas-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Docker up

#### Criar network
```bash
docker network create pessoas-network
```

#### API Pessoas backend

VUE_APP_API_PROXY é a rota para o backend, favor usar o ip local ou o nome do conteiner do backend na mesma rede

```bash
docker run -d --restart unless-stopped --name=pessoas-frontend \
 --network=pessoas-network --hostname=pessoas-frontend -p 8081:8081  \
 --log-opt max-size=10m --log-opt max-file=5 \
 -e VUE_APP_API_PROXY=http://pessoas-backend:8080 \
 leomaou/pessoas-frontend:dev
```