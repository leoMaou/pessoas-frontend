module.exports = {
    devServer: {
        port: 8081,
        proxy: process.env.VUE_APP_API_PROXY
    }
}