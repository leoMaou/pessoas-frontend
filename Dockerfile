FROM node:10.20.1-alpine3.9

RUN npm install -g http-server

RUN mkdir -p /data/node_modules
RUN mkdir -p /data

COPY /dist /data
COPY /node_modules /data/node_modules
COPY package*.json ./data/

RUN npm install

WORKDIR /data
EXPOSE 8081

RUN cd /data && npm i

ENTRYPOINT [ "http-server","-p","8081", "/data" ]