import { createWebHistory, createRouter } from "vue-router";

const routes =  [
  {
    path: "/",
    alias: "/pessoas",
    name: "pessoas",
    component: () => import("./components/PessoasList")
  },
  {
    path: "/pessoa",
    alias: "/pessoa/:id",
    name: "pessoa",
    component: () => import("./components/Pessoa")
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;