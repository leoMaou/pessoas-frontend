import http from "../http-common";

class PessoasDataService {

    getAll(cpf,nome) {
        let params = {};
        if(cpf) params.cpf = cpf;

        if(nome) params.nome = nome

        return http.get(`/api/v1/pessoas`, { params });
    }

    get(id) {
        return http.get(`/api/v1/pessoas/${id}`);
    }

    delete(id) {
        return http.delete(`/api/v1/pessoas/${id}`);
    }

    save(data) {
        if(data.id !== null) {
            return http.put(`/api/v1/pessoas/${data.id}`, data);
        }else {
            return http.post("/api/v1/pessoas", data);
        }
    }

}

export default new PessoasDataService();